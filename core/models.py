from django.db import models

from django.contrib.auth.models import User

# Create your models here.

STATE_CHOICES = (
    ('Done', 'DN' ),
    ('Doing', 'DG'),
    ('Not Stared', 'NS'),
)


class GrantGoal(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    grantgoal_name = models.CharField(max_length=64, default="Generic Grant Goal")
    description = models.CharField(max_length=256, default="Generic Grant Goal Description")
    timestamp = models.DateField(auto_now_add=True)
    final_date = models.DateField(auto_now_add=False, blank=True, null=True)
    update_date = models.DateField(auto_now=True)
    days_duration = models.IntegerField(default=7)
    status = models.BooleanField(default=True)
    state = models.CharField(max_length=16, choices=STATE_CHOICES)
    slug = models.SlugField(max_length=16)

    def __str__(self):
        return self.grantgoal_name